/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.builder;

import org.bitbucket.dtree.Decision;
import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.DecisionTree;
import org.bitbucket.dtree.Node;
import static org.bitbucket.dtree.builder.BuilderTest.BuildingType.FLAT;
import static org.bitbucket.dtree.builder.BuilderTest.BuildingType.HOUSE;
import static org.bitbucket.dtree.builder.BuilderTest.Material.BRICK;
import static org.bitbucket.dtree.builder.BuilderTest.Material.STONE;
import static org.bitbucket.dtree.builder.BuilderTest.Material.WOOD;
import static org.junit.Assert.assertSame;
import org.junit.Test;

/**
 *
 * @author count
 */
public class BuilderTest {
	
	public BuilderTest() {
	}
	
	enum BuildingType {
		HOUSE,
		FLAT
	}
	
	enum Material {
		WOOD,
		STONE,
		BRICK
	}
	
	
	@Test
	public void testSimple() {
		
		DecisionMaker<Boolean> dm = new DecisionMaker<Boolean>(){
			public Boolean getDecision() {
				return true;
			} 
		};
		
		Builder<String> b = Builder.forResult(String.class);
		
		DecisionTree t = b
			.tree()
			.result("foo");
		
		b.tree().with(dm).decide(
			b.on(true).result("yes"),
			b.on(false).result("no")
		);
	}
	/**
	 * 
	 * 
	 */
	@Test
	public void testSomeMethod() {
		
		Builder<String> b = Builder.forResult(String.class);
		DecisionTree<String> tree = b.tree()
			.with(houseOrFlatDialog())
			.decide(
				b.on(HOUSE)
					.with(materialDialog())
					.decide(
						b.on(WOOD).result("wooden house"),
						b.on(STONE).result("stone house"),
						b.on(BRICK).result("brick house")
					),
				b.on(FLAT)
					.result("flat")
			);
		
		assertSame(tree.getRoot().getType(), Node.Type.DECISION);
		assertSame(tree.getResultType(), String.class);
		Decision rootDecision = (Decision)tree.getRoot();
		
		assertSame(BuildingType.class, rootDecision.getKeyType());
	}
	
	DecisionMaker<BuildingType> houseOrFlatDialog() {
		return null;
	}
	
	DecisionMaker<Material> materialDialog() {
		return null;
	}
}
