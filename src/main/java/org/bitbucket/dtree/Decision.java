/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree;

import java.util.List;

/**
 *
 * @author count
 */
public interface Decision<R,T> extends Node<R> {
	Node<R> getNode(T key);
	List<Case<R,T>> getCases();

	public Class<?> getKeyType();
}
