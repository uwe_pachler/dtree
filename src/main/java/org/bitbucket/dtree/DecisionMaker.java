/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree;

/**
 *
 * @author count
 */
public interface DecisionMaker<T> {
	T getDecision();
}
