/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree;

/**
 *
 * @author count
 */
public interface Node<R> {
	enum Type {
		DECISION,
		RESULT
	}
	Type getType();
	
	Case<R,?> getParent();
}
