/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.builder;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.DecisionTree;
import org.bitbucket.dtree.impl.BuilderImpl;

/**
 *
 * @author count
 */
public abstract class Builder<X> {
	
	// factory for builder
	static <R> Builder<R> forResult(Class<R> clazz) {
		// FIXME: this is the only point where we link the /WRONG/ way to
		// the implementation. The correct way would be to use a SPI
		// with the ServiceLoader mechanism
		
		return new BuilderImpl<R>(clazz);
	}
	
	// create initial tree
	public abstract TreeBuilder<X> tree();
	
	// with | result
	public interface TreeBuilder<R> {
		<T> TreeWithBuilder<R,T> with(DecisionMaker<T> dm);
		DecisionTree<R> result(R r);
	}
	
	public interface TreeWithBuilder<R,T> {
		<T0> DecisionTree<R> decide(ClauseBuilder<R,T0> clause, ClauseBuilder<R,T0>... clauses);
	}
		
	public abstract <T> OnBuilder<X,T> on(T clauseValue);
	
	public interface OnBuilder<R,T0> {
		<T1> WithBuilder<R,T0,T1> with(DecisionMaker<T1> dm);
		ClauseBuilder<R,T0> result(R resultValue);
	}
	public interface WithBuilder<R,T0,T1> {
		ClauseBuilder<R,T0> decide(ClauseBuilder<R,T1> clause, ClauseBuilder<R,T1>... clauses);
	}
	
	public interface ClauseBuilder<R,T> {
	}
}
