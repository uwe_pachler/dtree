/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import java.util.Map;
import org.bitbucket.dtree.Case;

/**
 *
 * @author count
 */
public class CaseImpl<R,T> implements Case<R, T>{
	
	private DecisionImpl<R,?> parent;
	
	private final T key;
	private final NodeImpl<R> node;

	CaseImpl(T caseKey, NodeImpl<R> node) {
		this.key = caseKey;
		this.node = node;
		node.setParent(this);
	}
	
	void setParent(DecisionImpl<R,?> parent){
		this.parent = parent;
	}
	
	@Override
	public NodeImpl<R> getNode() {
		return node;
	}
	
	@Override
	public T getKey() {
		return key;
	}
	
}
