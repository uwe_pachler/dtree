/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.Case;
import org.bitbucket.dtree.Node;

/**
 *
 * @author upachler
 */
abstract class NodeImpl<R> implements Node<R>{
	private CaseImpl<R,?> parent;

	void setParent(CaseImpl<R, ?> parent) {
		this.parent = parent;
	}

	@Override
	public Case<R, ?> getParent() {
		return parent;
	}
	
}
