/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.builder.Builder;


/**
 *
 * @author upachler
 */
public class BuilderImpl<R> extends Builder<R> {
	
	private final Class<R> resultClass;
	
	public BuilderImpl(Class<R> resultClass) {
		this.resultClass = resultClass;
	}
	
	@Override
	public TreeBuilder<R> tree() {
		return new TreeBuilderImpl<R>(this);
	}

	@Override
	public <T> OnBuilderImpl<R,T> on(T clauseValue) {
		return new OnBuilderImpl<R,T>(clauseValue);
	}

	public Class<R> getResultClass() {
		return resultClass;
	}
	
}
