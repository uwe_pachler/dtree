/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.builder.Builder;

/**
 *
 * @author count
 */
public class WithBuilderImpl<R,T0,T1> extends AbstractWithBuilderImpl<R, T1> implements Builder.WithBuilder<R, T0, T1>{
	private final T0 onClauseKey;

	public WithBuilderImpl(T0 onClauseKey, DecisionMaker<T1> decisionMaker) {
		super(decisionMaker);
		this.onClauseKey = onClauseKey;
	}

	@Override
	public Builder.ClauseBuilder<R, T0> decide(Builder.ClauseBuilder<R, T1> clause, Builder.ClauseBuilder<R, T1>... clauses) {
		DecisionImpl<R, T1> decision = makeDecision(clause, clauses);
		return new ClauseBuilderImpl<>(new CaseImpl<>(onClauseKey, decision));
	}
	
}
