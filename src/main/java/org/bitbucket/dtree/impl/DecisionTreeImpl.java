/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionTree;
import org.bitbucket.dtree.Node;

/**
 *
 * @author upachler
 */
public class DecisionTreeImpl<R> implements DecisionTree<R>{

	private final NodeImpl<R> root;
	private final Class<R> resultType;

	public DecisionTreeImpl(Class<R> resultType, NodeImpl<R> root) {
		
		this.resultType = resultType;
		this.root = root;
	}
	
	
	@Override
	public Node<R> getRoot() {
		return root;
	}

	@Override
	public Class<R> getResultType() {
		return resultType;
	}
	
}
