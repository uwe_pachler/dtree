/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.builder.Builder.ClauseBuilder;

/**
 *
 * @author count
 */
class AbstractWithBuilderImpl<R,T> {
	private final DecisionMaker<T> decisionMaker;

	AbstractWithBuilderImpl(DecisionMaker<T> decisionMaker) {
		this.decisionMaker = decisionMaker;
	}

	protected DecisionImpl<R,T> makeDecision(ClauseBuilder clause, ClauseBuilder... clauses) {
		CaseImpl<R,T>[] cases = new CaseImpl[1+clauses.length];
		cases[0] = ((ClauseBuilderImpl)clause).getCase();
		for(int i=0; i<clauses.length; ++i) {
			cases[i+1] = ((ClauseBuilderImpl)clauses[i]).getCase();
		}
		
		return new DecisionImpl<R,T>(decisionMaker, cases);
	}
}
