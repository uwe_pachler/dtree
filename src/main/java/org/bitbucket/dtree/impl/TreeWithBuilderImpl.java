/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.builder.Builder.ClauseBuilder;
import org.bitbucket.dtree.builder.Builder.TreeWithBuilder;

/**
 *
 * @author count
 */
class TreeWithBuilderImpl<R,T> extends AbstractWithBuilderImpl<R, T> implements TreeWithBuilder<R, T>{
	
	private final BuilderImpl<R> parentBuilder;
	
	TreeWithBuilderImpl(BuilderImpl<R> parentBuilder, DecisionMaker<T> decisionMaker) {
		super(decisionMaker);
		this.parentBuilder = parentBuilder;
	}
	
	@Override
	public <T0> DecisionTreeImpl<R> decide(ClauseBuilder<R, T0> clause, ClauseBuilder<R, T0>... clauses) {
		DecisionImpl<R, T> decision = makeDecision(clause, clauses);
		return new DecisionTreeImpl<>(parentBuilder.getResultClass(), decision);
	}
	
}
