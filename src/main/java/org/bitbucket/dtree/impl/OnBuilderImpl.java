/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.builder.Builder;
import org.bitbucket.dtree.builder.Builder.ClauseBuilder;
import org.bitbucket.dtree.builder.Builder.WithBuilder;

/**
 *
 * @author count
 */
public class OnBuilderImpl<R,T> implements Builder.OnBuilder<R, T>{
	private final T clauseValue;

	OnBuilderImpl(T clauseValue) {
		this.clauseValue = clauseValue;
	}

	T getClauseValue() {
		return clauseValue;
	}
	
	@Override
	public <T1> WithBuilder<R, T, T1> with(DecisionMaker<T1> dm) {
		return new WithBuilderImpl<R,T,T1>(clauseValue, dm);
	}

	@Override
	public ClauseBuilderImpl<R, T> result(R resultValue) {
		CaseImpl<R,T> _case = new CaseImpl<R,T>(clauseValue, new ResultImpl<R>(resultValue));
		return new ClauseBuilderImpl(_case);
	}
	
}
