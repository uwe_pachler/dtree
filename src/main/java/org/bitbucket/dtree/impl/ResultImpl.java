/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.Result;

/**
 *
 * @author upachler
 */
class ResultImpl<R> extends NodeImpl<R>implements Result<R>{

	private final R resultValue;
	
	public ResultImpl(R r) {
		resultValue = r;
	}

	@Override
	public R getResultValue() {
		return resultValue;
	}

	@Override
	public Type getType() {
		return Type.RESULT;
	}
	
}
