/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.builder.Builder.TreeWithBuilder;
import org.bitbucket.dtree.builder.Builder.TreeBuilder;

/**
 *
 * @author upachler
 */
public class TreeBuilderImpl<R> implements TreeBuilder<R>{

	private final BuilderImpl parentBuilder;
	
	public TreeBuilderImpl(BuilderImpl parentBuilder) {
		this.parentBuilder = parentBuilder;
	}
	
	
	@Override
	public <T> TreeWithBuilderImpl<R,T> with(DecisionMaker<T> dm) {
		return new TreeWithBuilderImpl(parentBuilder, dm);
	}

	@Override
	public DecisionTreeImpl<R> result(R value) {
		ResultImpl<R> result = new ResultImpl<>(value);
		return new DecisionTreeImpl<>(parentBuilder.getResultClass(), result);
	}


	
}
