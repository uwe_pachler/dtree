/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.bitbucket.dtree.Case;
import org.bitbucket.dtree.Decision;
import org.bitbucket.dtree.DecisionMaker;
import org.bitbucket.dtree.Node.Type;

/**
 *
 * @author upachler
 */
class DecisionImpl<R, T> extends NodeImpl<R> implements Decision<R, T>{
	
	private final DecisionMaker<T> decisionMaker;
	private final CaseImpl<R, T>[] cases;
	private final Map<T, NodeImpl<R>> caseMap;

	public DecisionImpl(DecisionMaker<T> decisionMaker, CaseImpl<R,T>[] cases) {
		this.decisionMaker = decisionMaker;
		this.cases = cases;
		this.caseMap = new TreeMap<T, NodeImpl<R>>();
		for(CaseImpl<R,T> c : cases) {
			c.setParent(this);
			caseMap.put(c.getKey(), c.getNode());
		}
	}
	
	@Override
	public Type getType() {
		return Type.DECISION;
	}

	@Override
	public NodeImpl<R> getNode(T key) {
		return caseMap.get(key);
	}

	@Override
	public List<Case<R, T>> getCases() {
		return Arrays.asList(cases);
	}

	@Override
	public Class<?> getKeyType() {
		T key = cases[0].getKey();
		return key.getClass();
	}
	
	void addCase(CaseImpl _case) {
		
	}
	
}
