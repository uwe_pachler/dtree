/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.dtree.impl;

import org.bitbucket.dtree.builder.Builder.ClauseBuilder;

/**
 *
 * @author count
 */
class ClauseBuilderImpl<R,T> implements ClauseBuilder<R, T>{
	
	
	private final CaseImpl<R,T> _case;

	ClauseBuilderImpl(CaseImpl<R, T> _case) {
		this._case = _case;
	}
	
	
	CaseImpl<R,T> getCase() {
		return _case;
	}
}
