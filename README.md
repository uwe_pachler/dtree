# D-Tree #

D-Tree is a library for creating discrete decision trees. Such trees may be used to guide a user through a series of decisions, which eventually lead to a single outcome. Non-cyclic flow charts that have a single starting point can be represented as D-Trees. 

### How do I get set up? ###

* Check it out and get going...


Written in Markdown: [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)